<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'formidableinscription_description' => 'Outil qui permet de générer un pipeline d\'inscription lors de la saisie d\'un formulaire du plugin Formidable. 
	On peut ensuite utiliser les données envoyées par le pipeline pour les utiliser par exemple pour l\'inscription à une newsletter',
	'formidableinscription_nom' => 'Formulaire d\'inscription Formidable',
	'formidableinscription_slogan' => 'Extension du plugin formidable',
);