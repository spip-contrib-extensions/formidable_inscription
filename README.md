Formulaires d'inscription

Outil de développement

Se configure dans les traitements à la création d'un formulaire formidable.
S'utilise pour sélectionner des champs en correspondance,
de manière à traiter les données suivantes (tableau d'options)
via le pipeline traiter_formidableinscription

```
	'choix_inscription'
	'email'
	'nom' 
	'prenom' 
	'organisme' 
	'id_auteur'

```

Usage : par exemple pour une inscription à une newsletter, on récupérera le flux de traiter_formidableinscription (dans un autre plugin)
pour retrouver la réponse de 'choix_inscription' et 'email'